import React, { useState } from "react";

import "./ResinSetter.css";

let initialState = {
	hours: 0,
	minutes: 0,
	seconds: 0,
};

function ResinSetter() {
	const [formData, setFormData] = useState(initialState);

	const onChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const submit = (e) => {
		e.preventDefault();
		// Save date
		let now = new Date().getTime();
		now += 1000 * 60 * 60 * formData.hours;
		now += 1000 * 60 * formData.minutes;
		now += 1000 * formData.seconds;

		localStorage.setItem("maxResinDate", now);
		window.location.reload();
	};

	return (
		<React.Fragment>
			<form className="container">
				<h1>Input Full refresh times</h1>
				<div className="inputs">
					<input type="number" name="hours" onChange={onChange} value={formData.hours} />
					<input type="number" name="minutes" onChange={onChange} value={formData.minutes} />
					<input type="number" name="seconds" onChange={onChange} value={formData.seconds} />
				</div>
				<div className="labels">
					<span>Hours</span>
					<span>Minutes</span>
					<span>Seconds</span>
				</div>
				<input type="submit" value="Let's Go!!" onClick={submit} />
			</form>
		</React.Fragment>
	);
}

export default ResinSetter;
