import React from "react";
import { useNumberAnimator } from "../../hooks/useNumberAnimator";

function NumberDisplay({ number, className }) {
	const displayedNumber = useNumberAnimator(number);
	return <span className={className}>{displayedNumber || 0}</span>;
}

export default NumberDisplay;
