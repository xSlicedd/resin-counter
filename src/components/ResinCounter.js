import React, { useState } from "react";
import { MAX_RESIN } from "../utils";

import ResinButton from "./resinbutton/ResinButton";

import "./ResinCounter.css";
import Countdown from "./countdown/Countdown";
import NumberDisplay from "./numberdisplay/NumberDisplay";

function ResinCounter(props) {
	const [maxResinDate, setMaxResinDate] = useState(new Date(props.maxResinDate));
	const [resin, setResin] = useState(0);

	let fullDate = { date: maxResinDate.toLocaleDateString(), time: maxResinDate.toLocaleTimeString() };

	return (
		<div className="container">
			<section className="resin">
				<div className="amount">
					<img alt="resin" src="/Item_Fragile_Resin.png" />
					<NumberDisplay className={resin === MAX_RESIN ? "resin-count full" : "resin-count"} number={resin} />
				</div>
				<div className="buttons">
					<ResinButton cb={setMaxResinDate} type="remove" />
					<ResinButton cb={setMaxResinDate} type="add" />
				</div>
			</section>
			<Countdown {...{ setResin, resin, maxResinDate }} />
			<div className="fulldate">
				<span>Full:</span>
				<span>{fullDate.date}</span>
				<span>{fullDate.time}</span>
			</div>
		</div>
	);
}

export default ResinCounter;
