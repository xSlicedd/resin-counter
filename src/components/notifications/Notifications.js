import React, { useState, useEffect } from "react";

import "./Notifications.css";

function Notifications() {
	const [silent, setSilent] = useState("false");

	useEffect(() => {
		setSilent(localStorage.getItem("silentNotifs"));
	}, []);

	let changeSilent = (e) => {
		let isSilent = localStorage.getItem("silentNotifs");
		isSilent = isSilent === "true" ? "false" : "true";
		localStorage.setItem("silentNotifs", isSilent);
		setSilent(isSilent);
	};
	return (
		<div className="silent">
			<button className="btn-bell" onClick={changeSilent}>
				<i className={silent === "true" ? "fas fa-bell" : "far fa-bell"}></i>
			</button>
		</div>
	);
}

export default Notifications;
