import React from "react";

import "./Navbar.css";
import Notifications from "../notifications/Notifications";
import { Link } from "react-router-dom";

function Navbar() {
	const clear = () => {
		localStorage.removeItem("maxResinDate");
		window.location.reload();
	};

	return (
		<nav className="nav">
			<div className="nav-inner">
				<Link to="/">
					<div className="nav-logo">
						<img className="logo" src="/Item_Fragile_Resin.png" alt="Resin Tracker" />
						<span className="sitename">Resin Tracker</span>
					</div>
				</Link>
				<ul className="nav-links">
					<Notifications />
					<button onClick={clear}>Reset</button>
				</ul>
			</div>
		</nav>
	);
}

export default Navbar;
