import React from "react";
import { modifyResin } from "../../utils";

import "./ResinButton.css";

function ResinButton({ type, cb }) {
	let changeResin = () => {
		modifyResin(type, 20, cb);
	};

	return (
		<React.Fragment>
			<button onClick={changeResin} className={"btn-resin " + type}>
				<img className="resin-icon" alt="resin" src="/Item_Fragile_Resin.png" />
				<i className={type === "add" ? "fas fa-plus-circle" : "fas fa-minus-circle"}></i>
			</button>
		</React.Fragment>
	);
}

export default ResinButton;
