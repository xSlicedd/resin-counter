/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useRef } from "react";
import { calculateResin, MAX_RESIN, sendNotification } from "../../utils";

function Countdown({ resin, setResin, maxResinDate }) {
	const [seconds, setSeconds] = useState(0);
	const [minutes, setMinutes] = useState(8);

	const maxResinMs = maxResinDate.getTime();
	let interval = useRef();

	useEffect(() => {
		setTimes();
		interval.current = setInterval(setTimes, 1000);
		return () => {
			clearInterval(interval.current);
		};
	}, [maxResinDate]);

	function setTimes() {
		const now = new Date();
		const distance = maxResinMs - now.getTime();
		const res = calculateResin(distance);

		if (resin !== res) {
			setResin(res);
		}

		if (distance < 0) {
			//sendNotification(MAX_RESIN);
			setResin(MAX_RESIN);
			setSeconds("00");
			setMinutes("0");
			clearInterval(interval.current);
			return;
		}

		let minutesToResin = parseInt((distance / 60000) % 8);
		let secondsToResin = parseInt((distance / 1000) % 60);

		setMinutes(minutesToResin);
		setSeconds(secondsToResin < 10 ? `0${secondsToResin}` : secondsToResin);
	}

	return (
		<section className="resin-counter">
			<span className="minutes">{minutes}</span>
			<span className="colon">:</span>
			<span className="seconds">{seconds}</span>
		</section>
	);
}

export default Countdown;
