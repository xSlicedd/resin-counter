import React from "react";

import ResinCounter from "../../components/ResinCounter";
import ResinSetter from "../../components/resinsetter/ResinSetter";

function Resin() {
	let maxResinDate = null;
	if (localStorage.getItem("maxResinDate")) maxResinDate = parseInt(localStorage.getItem("maxResinDate"));

	return (
		<React.Fragment>{maxResinDate ? <ResinCounter maxResinDate={maxResinDate} /> : <ResinSetter />}</React.Fragment>
	);
}

export default Resin;
