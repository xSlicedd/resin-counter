/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";

export function useNumberAnimator(number) {
	// Re-run everytime the number parameter changes
	const [value, setValue] = useState(0);
	const incr = value < number;
	let change = Math.abs(value - number);

	useEffect(() => {
		let int = setInterval(() => {
			setValue((val) => {
				if (val !== number) return incr ? val + 1 : val - 1;
				else {
					clearInterval(int);
					return val;
				}
			});
		}, 500 / change);

		return () => {
			clearInterval(int);
		};
	}, [number]);

	return value;
}
