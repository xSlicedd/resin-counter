import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navbar from "./components/navbar/Navbar";
import Resin from "./routes/resin/Resin";

function App() {
	return (
		<React.Fragment>
			<Router>
				<Navbar />
				<Route exact path="/">
					<Resin />
				</Route>
				<Route exact path="/settings"></Route>
			</Router>
		</React.Fragment>
	);
}

export default App;
