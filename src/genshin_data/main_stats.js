export const DEF_PRC = {
	STAR_4: {
		BASE: 7.9,
		MAX: 43.5,
		GROWTH: (43.5 - 7.9) / 15,
	},
	STAR_5: {
		BASE: 8.7,
		MAX: 58.3,
		GROWTH: (58.3 - 8.7) / 19,
	},
};
export const HP_PRC = {
	STAR_4: {
		BASE: 6.3,
		MAX: 34.8,
		GROWTH: (34.8 - 6.3) / 15,
	},
	STAR_5: {
		BASE: 7.0,
		MAX: 46.6,
		GROWTH: (46.6 - 7.0) / 19,
	},
};
export const ATK_PRC = {
	STAR_4: {
		BASE: 6.3,
		MAX: 34.8,
		GROWTH: (34.8 - 6.3) / 15,
	},
	STAR_5: {
		BASE: 7.0,
		MAX: 46.6,
		GROWTH: (46.6 - 7.0) / 19,
	},
};
export const CRIT_RATE_PRC = {
	STAR_4: {
		BASE: 4.2,
		MAX: 23.2,
		GROWTH: (23.2 - 4.2) / 15,
	},
	STAR_5: {
		BASE: 4.7,
		MAX: 31.1,
		GROWTH: (31.1 - 4.7) / 19,
	},
};
export const CRIT_DMG_PRC = {
	STAR_4: {
		BASE: 8.4,
		MAX: 46.4,
		GROWTH: (46.4 - 8.4) / 15,
	},
	STAR_5: {
		BASE: 9.3,
		MAX: 62.2,
		GROWTH: (62.2 - 9.3) / 19,
	},
};
export const EL_MST = {
	STAR_4: {
		BASE: 25,
		MAX: 139,
		GROWTH: (139 - 25) / 15,
	},
	STAR_5: {
		BASE: 28,
		MAX: 187,
		GROWTH: (187 - 28) / 19,
	},
};
export const EL_DMG_PRC = {
	STAR_4: {
		BASE: 6.3,
		MAX: 34.8,
		GROWTH: (34.8 - 6.3) / 15,
	},
	STAR_5: {
		BASE: 7.0,
		MAX: 46.6,
		GROWTH: (46.6 - 7.0) / 19,
	},
};
export const PHYS_DMG_PRC = {
	STAR_4: {
		BASE: 7.9,
		MAX: 43.5,
		GROWTH: (43.5 - 7.9) / 15,
	},
	STAR_5: {
		BASE: 8.7,
		MAX: 58.3,
		GROWTH: (58.3 - 8.7) / 19,
	},
};
export const HEAL_BONUS_PRC = {
	STAR_4: {
		BASE: 4.8,
		MAX: 26.8,
		GROWTH: (26.8 - 4.8) / 15,
	},
	STAR_5: {
		BASE: 5.4,
		MAX: 35.9,
		GROWTH: (35.9 - 5.4) / 19,
	},
};
export const ENERGY_RECHARGE_PRC = {
	STAR_4: {
		BASE: 7.0,
		MAX: 38.7,
		GROWTH: (38.7 - 7.0) / 15,
	},
	STAR_5: {
		BASE: 7.8,
		MAX: 51.8,
		GROWTH: (51.8 - 7.8) / 19,
	},
};
export const BASE_ATK = {
	STAR_4: {
		BASE: 42,
		MAX: 232,
		GROWTH: (232 - 42) / 15,
	},
	STAR_5: {
		BASE: 47,
		MAX: 311,
		GROWTH: (311 - 47) / 19,
	},
};
export const BASE_HP = {
	STAR_4: {
		BASE: 645,
		MAX: 3571,
		GROWTH: (3571 - 645) / 15,
	},
	STAR_5: {
		BASE: 717,
		MAX: 4780,
		GROWTH: (4780 - 717) / 19,
	},
};
export const STATS = [
	{ value: "BASE_ATK", name: "ATK" },
	{ value: "BASE_HP", name: "HP" },
	{ value: "DEF_PRC", name: "DEF%" },
	{ value: "HP_PRC", name: "HP%" },
	{ value: "ATK_PRC", name: "ATK%" },
	{ value: "CRIT_RATE_PRC", name: "CRIT Rate%" },
	{ value: "CRIT_DMG_PRC", name: "CRIT DMG%" },
	{ value: "EL_MST", name: "Elemental Mastery" },
	{ value: "EL_DMG_PRC", name: "Elemental DMG%" },
	{ value: "PHYS_DMG_PRC", name: "Physical DMG%" },
	{ value: "HEAL_BONUS_PRC", name: "Healing Bonus%" },
	{ value: "ENERGY_RECHARGE_PRC", name: "Energy Recharge%" },
];
export const RARITIES = [
	{ value: "STAR_1", name: 1, maxLevel: 4 },
	{ value: "STAR_2", name: 2, maxLevel: 4 },
	{ value: "STAR_3", name: 3, maxLevel: 12 },
	{ value: "STAR_4", name: 4, maxLevel: 16 },
	{ value: "STAR_5", name: 5, maxLevel: 20 },
];

// export const TYPES = ["BASE", "MAX", "GROWTH"];
