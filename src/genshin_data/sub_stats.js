export let SUB_STATS = [
	{ value: "HP", name: "HP" },
	{ value: "ATK", name: "ATK" },
	{ value: "DEF", name: "DEF" },
	{ value: "EL_MST", name: "Elemental Mastery" },
	{ value: "ENERGY_RECHARGE_PRC", name: "Energy Recharge%" },
	{ value: "CRIT_RATE_PRC", name: "CRIT Rate%" },
	{ value: "CRIT_DMG_PRC", name: "CRIT DMG%" },
	{ value: "HEAL_BONUS_PRC", name: "Healing Bonus%" },
];
