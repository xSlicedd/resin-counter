export const MAX_RESIN = process.env.MAX_RESIN || 160;

export function calculateResin(distance) {
	let curResin = parseInt(MAX_RESIN - distance / (60000 * 8));
	return curResin;
}

// Gets the max Date and Time in Millis when Resin completely fills from 0
function getResinCapTimeFromNow(now) {
	return (now += 60000 * 8 * MAX_RESIN);
}

export function modifyResin(type, amount, setMaxResinDate) {
	const now = new Date().getTime();
	const date = parseInt(localStorage.getItem("maxResinDate"));
	const oldResin = calculateResin(date - now);

	let newDate;

	if (oldResin >= MAX_RESIN && type === "remove") {
		// Start fresh with 7:59 when old is higher or equal to MAX_RESIN, like the game
		newDate = now + 60000 * 8 * amount;
	} else {
		if (type === "add") {
			newDate = date - 1000 * 60 * (8 * amount);
			// if resin refill time is earlier than now its full, so set it to now
			if (newDate < now) newDate = now;
		} else {
			newDate = date + 1000 * 60 * (8 * amount);
			let cap = getResinCapTimeFromNow(now);
			// if resin refill time higher than maximum date use old maxResinDate so nothing changes
			if (newDate > cap) newDate = date;
		}
	}

	localStorage.setItem("maxResinDate", newDate);
	setMaxResinDate(new Date(newDate));
}

async function requestNotificationAccess() {
	if (Notification.permission !== "granted") {
		let res = await Notification.requestPermission();
		return res === "granted";
	}
	return true;
}

export function sendNotification(resin) {
	if (!window.Notification) return;
	if (Notification.permission === "granted") {
		let title = `You hit [${resin}] resin!`;
		new Notification(title, {
			body: "I suggest spending some resin in-game :)",
			icon: "Item_Fragile_Resin.png",
			silent: localStorage.getItem("silentNotifs") === "false",
		});
	} else requestNotificationAccess();
}
